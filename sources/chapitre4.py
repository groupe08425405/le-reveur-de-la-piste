import cgitb
import cgi
import csv

print("Content-type: text/html; charset=UTF_8\n")
cgitb.enable()
form = cgi.FieldStorage()
fichier = open('username.csv', 'r')# ouvre le fichier username.csv en mode lecture 
username = fichier.read() # assigne le contnue du fichier à la variable username
fichier.close()#ferme le fichier username.csv
choix2 = form.getvalue("choix2") #récupère la valeur renvoyer par la balise input dont l'attribut name est "choix2" et l'assigne à la variable choix2
fichier=open('choix2.csv','w') #ouvre le fichier choix2.csv en mode écriture
fichier.write(choix2)#écrit dans le fichier la valeur contenue dans la variable choix2
fichier.close#ferme le fichier choix2.csv 
fichier= open('choix1.csv','r') # ouvre le fichier choix1.csv en mode lecture
choix1 = fichier.read() ## assigne le contnue du fichier à la variable choix1
fichier.close#ferme le fichier choix1.csv
if choix1 == 'a': #si la valeur de la variable choix1  vaut "a"
    html1 = """ <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF_8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/style4.css"> 
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
</head>
<body>
    <h1>Le Rêveur de la Piste</h1>
    <h2>Chapitre 4 : Une malheureuse nouvelle</h2>
    <p>"""
    html2= """s'engage pleinement dans son apprentissage avec Jean. Ils passent des heures à perfectionner les techniques de conduite, à régler les moteurs, et à développer des stratégies pour les courses. Cependant, Jean tombe malade subitement, laissant """
    html3= """ seul avec la responsabilité de prendre les rênes de l'écurie. Doit-il reprendre les rênes ?
    </p>
    <form action="chapitre_intermediaire.py">
        <input type="radio" name="choix3" value="1" checked>Choix 1, Oui:(par défaut)
        <p>  Il perpétue l'enseignement de Jean.</p>
        <input type="radio" name="choix3" value="2">Choix 2, Non:
        <p>Il rejoint l’école renommée.</p>
        <input type="submit" value="page suivante" class="pagesuiv">
    </form>
</body>
</html>"""
# défini les différentes parties de la page pour intégrer le nom de personnage 
elif choix1 == 'b': #si la valeur de la variable choix1  vaut "b" 
    html1 = """ <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF_8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/style4.css"> 
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
</head>
<body>
    <h1>Le Rêveur de la Piste</h1>
    <h2>Chapitre 4 : Un parcours prometteur</h2>
    <p>À l'école de course, """
    html2= """ se distingue rapidement. Il est confronté à des compétitions féroces, mais il trouve aussi des alliés précieux. Un mentor secret commence à guider """
    html3=""" et il découvre que ce mentor n'est autre qu'un pilote légendaire retraité qui voit en lui un potentiel extraordinaire. Tout se passe pour le mieux dans le parcours du jeune pilote. </p>
    <form action="chapitre5.py">
    <input type="submit" value="page suivante" class="pagesuiv">
    </form>
</body>
</html>
    """
# défini les différentes parties de la page pour intégrer le nom de personnage 
print(html1)
print(username)
print(html2)
print(username)
print(html3)
# affiche les différentes parties de la page en intégrant le nom de personnage choisit par l'utilisateur