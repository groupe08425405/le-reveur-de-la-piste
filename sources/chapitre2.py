import cgitb
import cgi
import csv

print("Content-type: text/html; charset=UTF_8\n")
cgitb.enable()
form = cgi.FieldStorage()
fichier = open('username.csv', 'r') #ouvre le fichier username.csv en mode lecture
username = fichier.read() # assigne le contenu du fichier à la variable username
fichier.close() #ferme le fichier username.csv 
html="" # défini la variable html avant utilisation 
value_ecusson = form.getvalue("ecusson") #récupère la valeur renvoyer par la balise select dont l'attribut name est "ecusson" et l'assigne à la variable value_ecusson
if value_ecusson == "0": # si la valeur de la variable value_ecusson vaut 0 (si l'utilisateur n'a pas choisi d'écusson)
    html= """<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF_8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style/style4.css"> 
        <title>Document</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
    </head>
    </head>
    <body>
        <form method="post" action="chapitre1.py">
            <h2>Vous devez choisir un écusson.</h2>
            <input type="submit" value="Retour" class="retourverif">
        </form>
    </body>
    </html>""" # définie la valeur de la variable html (une page d'erreur qui renvoie l'utilisateur au choix de l'écusson)
    print(html) # affiche la page 
else : # dans les autres cas ( si l'utilisateur a bien choisit un écusson)
    fichier = open('ecusson.csv','w') # ouvre le fichier ecusson.csv en mode écriture 
    fichier.write(value_ecusson ) # écrit la valeur de la variable value_ecusson
    fichier.close() #ferme le fichier ecusson.csv
    html="""
        <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF_8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style/style4.css"> 
        <title>Document</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
    </head>
    <body>
        <form method="post" action="chapitre3.py">
        <h1>Le Rêveur de la Piste</h1>
        <h2>Chapitre 2 : Le Choix de la Voie</h2>
        <p>Un jour, alors que
        """
    html1 = """se promenait près de la piste, il rencontra un ancien pilote du nom de Jean. Il avait connu le succès dans sa jeunesse et décida de partager sa passion avec le jeune aspirant. Il proposa à 
    </html>
    """
    html2="""
    de devenir son apprenti, l'initiant ainsi aux mystères du monde de la course automobile.
            C'est à ce moment que l'histoire prend son sens. Quel chemin """
    html3= """ devrait-il emprunter pour réaliser son rêve ?
        </p>
                <input type="radio" name="choix1" value="a" checked>Choix A, Apprentissage avec Jean:(par défaut)
    <p>
       """
    html4="""accepte l'offre de Jean et commence un apprentissage traditionnel. Il travaille dur pour apprendre les techniques de conduite, la mécanique automobile et les stratégies de course.
    </p>
    <input type="radio" name="choix1" value="b">Choix B, Travailler seul: 
    <p>"""
    html5="""
        décline sèchement la proposition du vieux pilote et rentre dans une école de course de renom qui avait déjà repéré son talent.
    </p>
    <input type="submit" value="page suivante" class="pagesuiv">
     </form>
    </body>
    """ # défini les différentes parties de la page pour intégrer le nom de personnage 
    print(html)
    print(username)
    print(html1)
    print(username)
    print(html2)
    print(username)
    print(html3)
    print(username)
    print(html4)
    print(username)
    print(html5) 
    # affiche la page en intégrant le nom de personnage choisit par l'utilisateur 