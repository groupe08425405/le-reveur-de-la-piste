import cgitb
import cgi

fichier = open('username.csv', 'r') #ouvre le fichier username.csv en mode lecture
username = fichier.read() #assigne lecontenue du fichier à la variable username
fichier.close() #ferme le fichier username.csv
print("Content-type: text/html; charset=UTF_8\n")

cgitb.enable()
form = cgi.FieldStorage()

verif = "" 
action_url = ""
html=""
start=""
mid=""
end=""
#défini les variables avant utilisation 

verif_value = form.getvalue("verif") #récupère la valeur renvoyer par la balise input dont l'attribut name est "verif" et l'assigne à la variable verif_value

if verif_value == "oui": #si la variable verif_value vaut "oui"
    action_url = "chapitre2.py" # assigne la valeur "chapitre2.py" à la variable action_url
elif verif_value== "non": # si la variable verif_value vaut "non"
    action_url = "index.py" # assigne la valeur "index.py" à la variable action_url

form='<form method="post" action="">'
form = form.replace('action=""', f'action="{action_url}"') #remplace l'attribut action vide de la balise form par un attribut action dont la valeur est celle de la variable action_url
if action_url == "chapitre2.py": # si la variable action_url vaut "chapitre2.py"
    start = """<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF_8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style/style4.css"> 
        <title>Document</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
    </head>
    <body>
        <h1>Le Rêveur de la Piste</h1>
        <h2>Chapitre 1: Introduction</h2>
        <p>Il était une fois, dans une petite ville entourée de collines verdoyantes, un jeune garçon nommé
    """
    mid=""". Depuis son plus jeune âge, il était fasciné par les voitures qui rugissaient sur la piste de course juste à l'extérieur de la ville. Son cœur battait au rythme du vrombissement des moteurs, et ses yeux pétillaient d'excitation à chaque course.
            """
    end="""passait des heures à observer les pilotes chevronnés manœuvrer à des vitesses vertigineuses. Il rêvait secrètement de devenir l'un d'entre eux un jour. Les posters de voitures de course décoraient les murs de sa chambre, et il avait même construit une piste miniature dans son jardin pour faire rouler ses petites voitures. Mais, il lui manquait un écusson pour finir sa piste miniature.
        </p>"""

    print(start)
    print(username)
    print(mid)
    print(username)
    print(end)
    # affiche une partie de la page en coupant le code html au bon endroit pour intégrer le nom de personnage choisit par l'utilisateur 
    html="""   <label for="ecusson">Quel écusson devrait-il choisir ? (Pas important pour le reste de l'hisoire)</label> <br>
            <select name="ecusson" id="ecusson" required>
                <option value="0">Choisissez un écusson</option>
                <option value="images/Ecusson_1.png">Lightning R.C</option>
                <option value="images/Ecusson_2.png">Cold Piston R.C</option>
                <option value="images/Ecusson_3.png">Luxury R.C</option>
                <option value="images/Ecusson_4.png">Flamming Wheels R.C</option>
                <option value="images/Ecusson_5.png">Gracious Rose R.C</option>
            </select></br>
     <div class="imagerc">
            <img src="images/Ecusson_1.png" alt="ecusson1">
            <img src="images/Ecusson_2.png" alt="ecusson2">
            <img src="images/Ecusson_3.png" alt="ecusson3">
            <img src="images/Ecusson_4.png" alt="ecusson4">
            <img src="images/Ecusson_5.png" alt="ecusson5">
    </div>
            </br>
            <input type="submit" value="Valider" class="valider1">
        </form>
        </body>
        </html>""" # défini la valeur de la variable html
elif action_url == "index.py": #si action_url vaut "index.py"
    html="""<p>Modifie ton choix</p>
        <input type="submit" value="Retour" class="retour">
        </form>
        </body>
        </html>""" #définie la valeur de la variable html
else :  # dans tout les autres cas (sert quand l'utilisateur revient de la page chapitre2.py)
        form = form.replace('action=""', 'action="chapitre2.py"')
        start = """<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF_8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" href="style/style4.css"> 
            <title>Document</title>
            <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
        </head>
        <body>
            <h1>Le Rêveur de la Piste</h1>
            <h2>Chapitre 1: Introduction</h2>
            <p>Il était une fois, dans une petite ville entourée de collines verdoyantes, un jeune garçon nommé """
        mid=""". Depuis son plus jeune âge, il était fasciné par les voitures qui rugissaient sur la piste de course juste à l'extérieur de la ville. Son cœur battait au rythme du vrombissement des moteurs, et ses yeux pétillaient d'excitation à chaque course.
                """
        end="""passait des heures à observer les pilotes chevronnés manœuvrer à des vitesses vertigineuses. Il rêvait secrètement de devenir l'un d'entre eux un jour. Les posters de voitures de course décoraient les murs de sa chambre, et il avait même construit une piste miniature dans son jardin pour faire rouler ses petites voitures. Mais, il lui manquait un écusson pour finir sa piste miniature.
            </p>"""

        print(start)
        print(username)
        print(mid)
        print(username)
        print(end)
        # affiche une partie de la page en coupant le code html au bon endroit pour intégrer le nom de personnage choisit par l'utilisateur 
        html="""   <label for="ecusson">Quel écusson devrait-il choisir ? (Pas important pour le reste de l'hisoire)</label> <br>
            <select name="ecusson" id="ecusson">
                <option value="0">Choisissez un écusson</option>
                <option value="images/Ecusson_1.png">Lightning R.C</option>
                <option value="images/Ecusson_2.png">Cold Piston R.C</option>
                <option value="images/Ecusson_3.png">Luxury R.C</option>
                <option value="images/Ecusson_4.png">Flamming Wheels R.C</option>
                <option value="images/Ecusson_5.png">Gracious Rose R.C</option>
            </select>
    <div class="imagerc">
            <img src="images/Ecusson_1.png" alt="ecusson1">
            <img src="images/Ecusson_2.png" alt="ecusson2">
            <img src="images/Ecusson_3.png" alt="ecusson3">
            <img src="images/Ecusson_4.png" alt="ecusson4">
            <img src="images/Ecusson_5.png" alt="ecusson5">
    </div>
            <input type="submit" value="Valider" class="valider1">
        </form>
        </body>
        </html>"""# défini la valeur de la variable html
print(form)
print(html) #affiche les parties manquantes de la page