import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="style/style4.css">
	<title> Ma page </title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
</head>
<body>
    <h1> Bienvenue</h1>
	<form method="post" action="page_verif.py">
    <label for="username" class="verif">Quel est le prénom de votre personnage ?</label><input id="username" type="text" name="username"><br><br>
    <input type="submit" value="Page suivante" class="pagesuivindex">
</body>
</html>
""" #défini le contenue de la page d'accueil dans laquelle l'utilisateur doit choisir un nom de personnage
print(html)