import cgitb
import cgi
import csv

print("Content-type: text/html; charset=UTF_8\n")
cgitb.enable()
form = cgi.FieldStorage()
fichier = open('username.csv', 'r') # ouvre le fichier username.csv en mode lecture
username = fichier.read() # # assigne le contnue du fichier à la variable username
fichier.close() # ferme le fichier username.csv 
fichier = open('choix2.csv','r') #ouvre le fichier choix2.csv en mode lecture
choix2 = fichier.read()# assigne le contenue du fichier à la variable choix2
fichier.close()#ferme le fichier choix2.csv 
if choix2 == "c":# si la valeur contenue dans la variable choix2 vaut "c"
    html1="""<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF_8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/style4.css"> 
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
</head>
<body>
    <h1>Le Rêveur de la Piste</h1>
    <h2>Chapitre 5 : Les Épreuves du Parcours</h2>
    <p>L'amitié entre """
    html2=""" et son rival Lucas, se renforce au fil des courses. Cependant, à un moment crucial, Lucas se retrouve en difficulté, et """
    html3=""" doit choisir entre la victoire et l'amitié. Cette décision aura des répercussions sur leur relation et sur sa carrière. Doit-il l'aider ?</p>
    <form action="chapitre_final.py">
        <input type="radio" name="choix4" value="e" checked>Choix E:(par défaut)
        <p>"""
    html4=""" sacrifie la victoire pour aider Lucas, mais cela crée des tensions avec son équipe et ses sponsors. Il doit maintenant se battre pour prouver que l'amitié vaut plus que la gloire.
        </p>
        <input type="radio" name="choix4" value="f" >Choix F:
        <p> Lorsque Lucas se retrouve en difficulté sur la piste, """
    html5=""" choisit de l'ignorer pour se concentrer sur la victoire. Cette décision égoïste compromet non seulement leur amitié mais aussi sa réputation. </p>
        <input type="submit" value="page suivante" class="pagesuiv">
    </form>
</body>
</html>"""
# défini les différentes parties de la page pour intégrer le nom de personnage 
elif choix2 == "d": # si la valeur contenue dans la variable choix2 vaut "d"
    html1="""<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF_8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/style4.css"> 
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
</head>
<body>
    <h1>Le Rêveur de la Piste</h1>
    <h2>Chapitre 5 : Les Épreuves du Parcours</h2>
    <p>Alors que"""
    html2=""" explorait les archives de son grenier, il découvre que son propre père était autrefois un pilote de renom. Il avait disparu mystérieusement lors d'une course il y a des années. """
    html3=""" est maintenant confronté à la décision de poursuivre cette piste ou de se concentrer sur sa propre carrière.
        </p>
    <form action="chapitre_final.py">
        <input type="radio" name="choix4" value="g">Choix G:
        <p> En suivant les indices, """
    html4=""" découvre que son père est toujours en vie, mais il a choisi de se retirer du monde de la course. Il décide alors de tout plaquer pour retrouver son père.</p>
        <input type="radio" name="choix4" value="h">Choix H:
        <p> """
    html5=""" découvre des informations cruciales sur le passé de son père, mais choisit délibérément de les ignorer pour ne pas perturber sa propre carrière.</p>
        <input type="submit" value="page suivante" class="pagesuiv">
    </form>
</body>
</html>"""
# défini les différentes parties de la page pour intégrer le nom de personnage 

print(html1)
print(username)
print(html2)
print(username)
print(html3)
print(username)
print(html4)
print(username)
print(html5)
# affiche les différentes parties de la page en intégrant le nom de personnage choisit par l'utilisateur