import cgitb
import cgi

print("Content-type: text/html; charset=UTF_8\n")
#défini la première partie de la page
start = """<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF_8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/style4.css"> 
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
</head>
<body>
"""
print(start) 
cgitb.enable()
form = cgi.FieldStorage()

verif = "" #défini la variable "verif" utilisée plus tard
try: #test 
    if form.getvalue("username"): #si l'utilisateur a entré un nom de personnage 
        username = form.getvalue("username") #récupère le nom entré
        fichier = open(file="username.csv", mode="w") #ouvre le fichier username.csv en mode écriture
        fichier.write(username) #écrit le nom choisit dans le fichier csv 
        fichier.close() #ferme le fichier csv
        verif = """<form method="post" action="chapitre1.py" id="myForm">
            <p class="verif">Êtes-vous sûr du nom de votre personnage ?</p>
            <input type="radio"  value="non" name="verif" class="ouinon" required> Non 
            <input type="radio"  value="oui" name="verif" class="ouinon" required> Oui
            <p class="verif">L'histoire va commencer, vous ne pourrez plus le modifier.</p>
            <input type="submit" value="Page suivante" class="pagesuivverif">
        """ #défini la deuxième partie de la page
    else: # sinon lève une exception
        raise Exception
except: # si une exception est levée 
    print("""<p class="verif">Veuillez entrer un nom de personnage!</p>""") # dit à l'utilisateur de rentrer un nom de personnage
    verif="""<form method="post" action="index.py" >
             <input type="submit" value="Retour" class="retourverif">""" #défini la deuxième partie de la page avec un bouton qui renvoie à la page d'accueil 
print(verif)
end = """
</form>
</body>
</html>
""" # défini la fin de la page
print(end)
