import http.server
# choisir un port et indiquer l'IP du serveur
port = 80
address=("", port)

# le serveur
server = http.server.HTTPServer
# Le gestionnaire fonctionne avec l'interface de scripts CGI
handler = http.server.CGIHTTPRequestHandler
# Les scripts seront dans le répertoire racine directement
handler.cgi_directories=["/"]

httpd = server(address, handler)
print(f"Serveur démarré sur le PORT{port}")
httpd.serve_forever()