import cgitb
import cgi
import csv

print("Content-type: text/html; charset=UTF_8\n")
cgitb.enable()
form = cgi.FieldStorage()
fichier = open('username.csv', 'r') # ouvre le fichier username.csv en mode lecture 
username = fichier.read() # assigne le contenu du fichier à la variable username
fichier.close() # ferme le fichier username.csv
choix1 = form.getvalue("choix1") #récupère la valeur renvoyer par la balise input dont l'attribut name est "choix1" et l'assigne à la variable choix1
fichier=open('choix1.csv','w') #ouvre le fichier choix1.csv en mode écriture
fichier.write(choix1) # écrit la valeur de la variable choix1 dans le fichier
fichier.close #ferme le fichier choix1.csv
html="""<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF_8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/style4.css"> 
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
</head>
<body>
    <h1>Le Rêveur de la Piste</h1>
    <h2>Chapitre 3 : Les Défis de la Course</h2>
    <p>Quel que soit le chemin choisi,"""
html1="""  doit maintenant affronter les défis de la course automobile. Des rivalités naissent sur la piste, des amitiés se forgent, et il doit prendre des décisions cruciales pour surmonter les obstacles. À ce stade, vous pouvez choisir l'un des deux développements suivants :
    </p>
    <form action="chapitre4.py">
        <input type="radio" name="choix2" value="c" checked>Choix C, Rivalité Amicale :(par défaut)
        <p>"""
html2="""se lie d'amitié avec Lucas, un autre pilote talentueux. Leur rivalité les poussent à atteindre de nouveaux sommets, mais cela met aussi à l'épreuve l'amitié qui se crée entre les deux jeunes pilotes.</p>
        <input type="radio" name="choix2" value="d">Choix D, Secrets du Passé :
        <p>En fouillant dans des archives de son grenier, """
html3="""découvre un mystérieux secret du passé qui pourrait changer sa carrière et décide de le percer à jour. 
        </p>
        <input type="submit" value="page suivante" class="pagesuiv">
    </form>
</body>
</html>"""
 # défini les différentes parties de la page pour intégrer le nom de personnage 
print(html)
print(username)
print(html1)
print(username)
print(html2)
print(username)
print(html3)
# affiche la page en intégrant le nom de personnage choisit par l'utilisateur 