import cgitb
import cgi
import csv

print("Content-type: text/html; charset=UTF_8\n")
cgitb.enable()
form = cgi.FieldStorage()
fichier = open('username.csv', 'r')#ouvre le fichier username.csv en mode lecture
username = fichier.read() # assigne le contenu du fichier à la variable username
fichier.close()# ferme le fichier username.csv
choix3= form.getvalue("choix3")#récupère la valeur renvoyer par la balise input dont l'attribut name est "choix3" et l'assigne à la variable choix3
fichier=open('choix3.csv','w')# ouvre le fichier choix3.csv en mode écriture
fichier.write(choix3)# écrit dans le fichier le contenue de la variable choix3
fichier.close()#ferme le fichier choix3.csv
if choix3 == '1': # si la valeur contenue dans la variable choix3 vaut "1"
    html1="""<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF_8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style/style4.css"> 
        <title>Document</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
    </head>
    <body>
        <h1>Le Rêveur de la Piste</h1>
        <h2>Chapitre intermédiaire : Décés de Jean</h2>
        <p>"""
    html2=""" se retrouve seul au sein de l’écurie dans laquelle Jean l’a accueilli plus tôt, malgré sa solitude, """
    html3=""" poursuit son entraînement en suivant les méthodes de Jean. Motivé à ne pas décevoir son ancien mentor, il fait preuve d’une détermination, et d’une rigueur époustouflante.</p>
        <form action="chapitre5.py">
        <input type="submit" value="page suivante" class="pagesuivichapinter">
        </form>
    </body>
    </html>"""
    # défini les différentes parties de la page pour intégrer le nom de personnage 

elif choix3 == '2':# si la valeur contenue dans la variable choix3 vaut "2"
    html1="""<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF_8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/style4.css"> 
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
</head>
<body>
    <h1>Le Rêveur de la Piste</h1>
    <h2>Chapitre intermédiaire : Décés de Jean</h2>
    <p>"""
    html2=""", désemparé suite à la perte de Jean, décide de rejoindre l’école de course qui l’avait repéré plus tôt. Malgré cette épreuve de la vie """
    html3=""" excelle toujours et trouve même un nouveau mentor. Par chance, celui-ci s’avère être lui aussi un ancien pilote.</p>
    <form action="chapitre5.py">
        <input type="submit" value="page suivante" class="pagesuivichapinter">
    </form>
</body>
</html>"""
# défini les différentes parties de la page pour intégrer le nom de personnage 

print(html1)
print(username)
print(html2)
print(username)
print(html3)
# affiche les différentes parties de la page en intégrant le nom de personnage choisit par l'utilisateur
