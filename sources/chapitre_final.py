import cgitb
import cgi
import csv

print("Content-type: text/html; charset=UTF_8\n")
cgitb.enable()
form = cgi.FieldStorage()
fichier = open('username.csv', 'r')#ouvre le fichier username.csv en mode lecture 
username = fichier.read()#assigne le contenu du fichier à la variable username
fichier.close()#ferme le fichier username.csv
fichier = open('choix1.csv','r')#ouvre le fichier choix1.csv en mode lecture 
choix1 = fichier.read()#assigne le contenue du fichier à la variable choix2
fichier.close()#ferme le fichier choix1.csv
fichier = open('choix2.csv','r')#ouvre le fichier choix2.csv en mode lecture 
choix2 = fichier.read()#assigne le contenue du fichier à la variable choix2
fichier.close()#ferme le fichier choix2.csv
choix4= form.getvalue("choix4")#récupère la valeur renvoyer par la balise input dont l'attribut name est "choix4" et l'assigne à la variable choix4
fichier = open('choix4.csv','w')#ouvre le fichier choix4.csv en mode lecture 
fichier.write(choix4)#écrit dans le fichier le contenu de la variable choix4
fichier.close()#ferme le fichier choix4.csv
fin = ""
html1=""
html2=""
#défini les variables avant utilisation
if choix2 == "c" and choix4 == "e":
    fin = "fin_1"
elif choix2 == "d" and choix4 == "h":
    fin = "fin_2"
elif choix2 == "d" and choix4 == "g":
    fin = "fin_3"
elif choix2 == "c" and choix4 == "f":
    fin = "fin_4"
#détermine la fin en fonction des choix faits par l'utlilisateur au cours de l'histoire
if fin == "fin_1":#si la fin 1 est celle apropriée à l'histoire de l'utilisateur
    if choix1=="a":
        html1 = """<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF_8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" href="style/style4.css"> 
            <title>Document</title>
            <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
        </head>
        <body>
            <h1> Le Rêveur de la Piste </h1>
            <h2>Chapitre final: Fin n°1 : La Légende de l'Amitié</h2>
            <p>"""
        html2=""", ayant choisi la voie de l'apprentissage traditionnel avec Jean, a développé une amitié solide avec Lucas. Face à la difficulté, ils ont formé une équipe imbattable. Ensemble, ils ont remporté des victoires mémorables, prouvant que l'amitié peut être tout aussi puissante que la compétition. """
        html3=""", Lucas et indirectement Jean, ont créé une légende qui perdurera dans le monde de la course automobile.</p>
            <form action="index.py" method="post">
                <input type="submit" value="Commencer une nouvelle histoire" class="nouvellehist1">
            </form>
        </body>
        </html>"""
        # défini les différentes parties de la page pour intégrer le nom de personnage 

        print(html1)
        print(username)
        print(html2)
        print(username)
        print(html3)
        # affiche les différentes parties de la page en intégrant le nom de personnage choisit par l'utilisateur
    elif choix1=="b":
        html1 = """<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF_8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" href="style/style4.css"> 
            <title>Document</title>
            <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
        </head>
        <body>
            <h1> Le Rêveur de la Piste </h1>
            <h2>Chapitre final: Fin n°1 : La Légende de l'Amitié</h2>
            <p>"""
        html2=""", ayant choisi d'intégrer cette école de pilotage prestigieuse a pu développé une amitié solide avec Lucas. Face à la difficulté, ils ont formé une équipe imbattable. Ensemble, ils ont remporté des victoires mémorables, prouvant que l'amitié peut être tout aussi puissante que la compétition. Lucas, """
        html3="""  et son mentor ont créé une légende qui perdurera dans le monde de la course automobile.</p>
            <form action="index.py" method="post">
                <input type="submit" value="Commencer une nouvelle histoire" class="nouvellehist1">
            </form>
        </body>
        </html>"""
        # défini les différentes parties de la page pour intégrer le nom de personnage 

        print(html1)
        print(username)
        print(html2)
        print(username)
        print(html3)
        # affiche les différentes parties de la page en intégrant le nom de personnage choisit par l'utilisateur
elif fin == "fin_2":#si la fin 2 est celle apropriée à l'histoire de l'utilisateur
    html1 = """<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF_8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style/style4.css"> 
        <title>Document</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
    </head>
    <body>
        <h1> Le Rêveur de la Piste </h1>
        <h2>Chapitre final: Fin n°2 :  La Voie de la Gloire</h2>
        <p>Choisissant l'école de course renommée, """
    html2=""" est rapidement devenu une étoile montante. Guidé par son mystérieux mentor, il remporte des championnats, accumule des trophées, et sa carrière atteint des sommets inimaginables. Cependant, son mentor révèle finalement son identité : c'était son père, revenu discrètement pour guider son fils vers la gloire.</p>
        <form action="index.py" method="post">
            <input type="submit" value="Commencer une nouvelle histoire" class="nouvellehist2">
        </form>
    </body>
    </html>"""
    # défini les différentes parties de la page pour intégrer le nom de personnage 

    print(html1)
    print(username)
    print(html2)
    # affiche les différentes parties de la page en intégrant le nom de personnage choisit par l'utilisateur
elif fin == "fin_3": #si la fin 3 est celle apropriée à l'histoire de l'utilisateur
    html1 = """<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF_8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style/style4.css"> 
        <title>Document</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
    </head>
    <body>
        <h1> Le Rêveur de la Piste </h1>
        <h2>Chapitre final: Fin n°3 :  Réunir les Morceaux du Passé</h2>
        <p>"""
    html2=""", attiré par les mystères du passé familial, choisit de suivre la piste de son père disparu. En découvrant qu'il est en vie, """
    html3=""" prend la décision courageuse de le retrouver. Les retrouvailles sont émouvantes, le père et le fils décident de partager la piste une dernière fois. Cela marque la fin d'une quête personnelle et le début d'un nouveau chapitre pour la famille.</p>
        <form action="index.py" method="post">
            <input type="submit" value="Commencer une nouvelle histoire" class="nouvellehist3">
        </form>
    </body>
    </html>"""
    # défini les différentes parties de la page pour intégrer le nom de personnage 
    print(html1)
    print(username)
    print(html2)
    print(username)
    print(html3)
    # affiche les différentes parties de la page en intégrant le nom de personnage choisit par l'utilisateur
elif fin == "fin_4": #si la fin 4 est celle apropriée à l'histoire de l'utilisateur
    html1 = """<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF_8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style/style4.css"> 
        <title>Document</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200..700&display=swap" rel="stylesheet">
    </head>
    <body>
        <h1> Le Rêveur de la Piste </h1>
        <h2>Chapitre final: Fin n°4 :  La Perte Totale </h2>
        <p>Le chemin sombre choisi par """
    html2=""" a laissé derrière lui un sillage de trahison, d'égocentrisme et d'ignorance. Sa soif de victoire à tout prix l'a conduit à des choix destructeurs, et le prix à payer est bien plus élevé que ce qu'il aurait pu imaginer.
            Après avoir trahi son ami Lucas sur la piste, """
    html3=""" a non seulement perdu la course, mais aussi sa plus belle amitié. Les tensions sur le circuit se sont transformées en scandale, et la réputation de """
    html4=""" a été ternie de manière irréparable. Les sponsors se retirent, les équipes se désolidarisent, et """
    html5=""" se retrouve seul, confronté à l'ampleur de ses propres erreurs.
            Ignorer l'appel à l'aide de Lucas a été la goutte d'eau qui fait déborder le vase. Les médias se déchaînent, soulignant le manque de fair-play et de camaraderie de """
    html6=""". Le public déçu, les fans se détournent, et même les admirateurs les plus fidèles commencent à douter.
            Pire encore, en ignorant les signaux d'alerte concernant le passé de son père, """
    html7=""" a déclenché une série d'événements qui ont mis en péril la vie de sa famille. Les révélations, qu'il aurait pu utiliser pour unir sa famille, ont été ignorées au profit d'une carrière éphémère.
            Dans l'isolement qui s'ensuit, """
    html8=""" se retrouve sans carrière, sans amis, et sans famille solide. La gloire qu'il cherchait n'est qu'un songe lointain, et il est confronté à une vérité amère : parfois, la route vers le sommet peut être pavée de choix regrettables et de pertes incommensurables.
            </p>
        <form action="index.py" method="post">
            <input type="submit" value="Commencer une nouvelle histoire" class="nouvellehist4">
        </form>
    </body>
    </html>"""
    # défini les différentes parties de la page pour intégrer le nom de personnage 

    print(html1)
    print(username)
    print(html2)
    print(username)
    print(html3)
    print(username)
    print(html4)
    print(username)
    print(html5)
    print(username)
    print(html6)
    print(username)
    print(html7)
    print(username)
    print(username)
    print(html8)
    # affiche les différentes parties de la page en intégrant le nom de personnage choisit par l'utilisateur